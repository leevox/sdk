﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Leevox.Ums
{
    public class Application
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Application Parent { get; set; }
        public IEnumerable<Application> Children { get; set; }
        public string Url { get; set; }
        public string PublicKey { get; set; }

        internal DateTime Created { get; set; }
        internal DateTime Updated { get; set; }
        internal DateTime Deleted { get; set; }

        private static IList<Application> AllApplications { get; set; }

        static Application()
        {
            AllApplications = new List<Application>();
        }

        public Application this[string name]
        {
            get
            {
                return
                    AllApplications.FirstOrDefault(
                        x => string.Compare(x.Name, name, StringComparison.CurrentCultureIgnoreCase) == 0
                    );
            }
        }
    }
}
