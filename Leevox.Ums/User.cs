﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace Leevox.Ums
{
    public class User
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public User Parent { get; set; }
        public IEnumerable<User> Children { get; set; }
        internal string Password { get; set; }

        internal DateTime Created { get; set; }
        internal DateTime Updated { get; set; }
        internal DateTime Deleted { get; set; }

        private static IList<User> AllUsers { get; set; }

        static User()
        {
            AllUsers = new List<User>();
        }

        public User this[string userName]
        {
            get
            {
                return
                    AllUsers.FirstOrDefault(
                        x => string.Compare(x.UserName, userName, StringComparison.CurrentCultureIgnoreCase) == 0
                    );
            }
        }

        public User this[long userId]
        {
            get
            {
                return
                    AllUsers.FirstOrDefault(
                        x => x.Id == userId
                    );
            }
        }
    }
}
