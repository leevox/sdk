CREATE DATABASE [UMS]
GO
USE [UMS]
GO

CREATE LOGIN [UMSGateway] WITH PASSWORD = '123456', DEFAULT_DATABASE = [UMS], CHECK_POLICY = OFF, CHECK_EXPIRATION = OFF
GO

/****** Object:  StoredProcedure [dbo].[ApplicationAuthenticate]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ApplicationAuthenticate]
	@ApplicationId int,
	@Username nvarchar(50),
	@Password nvarchar(50)
AS BEGIN
	Declare @pwd nvarchar(50) = ''
	Declare @uid bigint = -1;
	Declare @md5 nvarchar(50) = CONVERT(NVARCHAR(50), HASHBYTES('MD5', @Password), 2)

	SELECT @uid = Id
	FROM [User]
	WHERE UserName = @Username

	SELECT @pwd = [Password]
	FROM ApplicationUser
	WHERE ApplicationId = @ApplicationId
		AND UserId = @uid

	IF SUBSTRING(@md5, 1, 10) + SUBSTRING(@md5, 15, 15) = @pwd
	BEGIN
		SELECT *
		FROM [Application]
		WHERE Id = @ApplicationId
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Authenticate]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Authenticate]
	@Username nvarchar(50),
	@Password nvarchar(50)
AS BEGIN
	Declare @pwd nvarchar(50) = ''
	Declare @uid bigint = -1;
	Declare @md5 nvarchar(50) = CONVERT(NVARCHAR(50), HASHBYTES('MD5', @Password), 2)

	SELECT @uid = Id
	FROM [User]
	WHERE UserName = @Username

	SELECT @pwd = [Password]
	FROM [User]
	WHERE Id = @uid

	IF SUBSTRING(@md5, 1, 10) + SUBSTRING(@md5, 15, 15) = @pwd
	BEGIN
		SELECT *
		FROM [Application]
		WHERE Id IN (
			SELECT ApplicationId
			FROM ApplicationUser
			WHERE UserId = @uid
		)
	END
END
GO
/****** Object:  Table [dbo].[Application]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Parent] [int] NULL,
	[PublicKey] [nvarchar](50) NOT NULL,
	[Url] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationPermission]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationPermission](
	[Id] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[PermisisonId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_ApplicationPermission] UNIQUE NONCLUSTERED 
(
	[ApplicationId] ASC,
	[PermisisonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationRole]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationRole](
	[Id] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Parent] [int] NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_ApplicationRole] UNIQUE NONCLUSTERED 
(
	[ApplicationId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationUser]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUser](
	[Id] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Password] [nvarchar](50) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationUserId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_ApplicationUser] UNIQUE NONCLUSTERED 
(
	[ApplicationId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationUserPermission]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUserPermission](
	[Id] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[ApplicationPermissionId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationUserPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_ApplicationUserPermission] UNIQUE NONCLUSTERED 
(
	[ApplicationUserId] ASC,
	[ApplicationPermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplicationUserRole]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUserRole](
	[Id] [int] NOT NULL,
	[ApplicationUserId] [int] NOT NULL,
	[ApplicationRoleId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_ApplicationUserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_ApplicationUserRole] UNIQUE NONCLUSTERED 
(
	[ApplicationUserId] ASC,
	[ApplicationRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [int] NOT NULL,
	[ApplicationId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_PermissionId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] NOT NULL,
	[Parent] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_RoleId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] NOT NULL,
	[ManagerId] [bigint] NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](200) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_UserId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserPermission]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserPermission](
	[Id] [int] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_UserPermission] UNIQUE NONCLUSTERED 
(
	[UserId] ASC,
	[PermissionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 12/30/2014 1:26:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] NOT NULL,
	[UserId] [bigint] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [uc_UserRole] UNIQUE NONCLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Application]  WITH CHECK ADD  CONSTRAINT [FK_Application_Application] FOREIGN KEY([Parent])
REFERENCES [dbo].[Application] ([Id])
GO
ALTER TABLE [dbo].[Application] CHECK CONSTRAINT [FK_Application_Application]
GO
ALTER TABLE [dbo].[ApplicationPermission]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationPermission_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO
ALTER TABLE [dbo].[ApplicationPermission] CHECK CONSTRAINT [FK_ApplicationPermission_Application]
GO
ALTER TABLE [dbo].[ApplicationPermission]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationPermission_Permission] FOREIGN KEY([PermisisonId])
REFERENCES [dbo].[Permission] ([Id])
GO
ALTER TABLE [dbo].[ApplicationPermission] CHECK CONSTRAINT [FK_ApplicationPermission_Permission]
GO
ALTER TABLE [dbo].[ApplicationRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationRole_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO
ALTER TABLE [dbo].[ApplicationRole] CHECK CONSTRAINT [FK_ApplicationRole_Application]
GO
ALTER TABLE [dbo].[ApplicationRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[ApplicationRole] CHECK CONSTRAINT [FK_ApplicationRole_Role]
GO
ALTER TABLE [dbo].[ApplicationRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationRoleParent] FOREIGN KEY([Parent])
REFERENCES [dbo].[ApplicationRole] ([Id])
GO
ALTER TABLE [dbo].[ApplicationRole] CHECK CONSTRAINT [FK_ApplicationRoleParent]
GO
ALTER TABLE [dbo].[ApplicationUser]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUser_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUser] CHECK CONSTRAINT [FK_ApplicationUser_Application]
GO
ALTER TABLE [dbo].[ApplicationUser]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUser_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUser] CHECK CONSTRAINT [FK_ApplicationUser_User]
GO
ALTER TABLE [dbo].[ApplicationUserPermission]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUserPermission_ApplicationPermission] FOREIGN KEY([ApplicationPermissionId])
REFERENCES [dbo].[ApplicationPermission] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUserPermission] CHECK CONSTRAINT [FK_ApplicationUserPermission_ApplicationPermission]
GO
ALTER TABLE [dbo].[ApplicationUserPermission]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUserPermission_ApplicationUser] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[ApplicationUser] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUserPermission] CHECK CONSTRAINT [FK_ApplicationUserPermission_ApplicationUser]
GO
ALTER TABLE [dbo].[ApplicationUserRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUserRole_ApplicationRole] FOREIGN KEY([ApplicationRoleId])
REFERENCES [dbo].[ApplicationRole] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUserRole] CHECK CONSTRAINT [FK_ApplicationUserRole_ApplicationRole]
GO
ALTER TABLE [dbo].[ApplicationUserRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUserRole_ApplicationUser] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[ApplicationUser] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUserRole] CHECK CONSTRAINT [FK_ApplicationUserRole_ApplicationUser]
GO
ALTER TABLE [dbo].[Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_Application] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[Application] ([Id])
GO
ALTER TABLE [dbo].[Permission] CHECK CONSTRAINT [FK_Permission_Application]
GO
ALTER TABLE [dbo].[Role]  WITH CHECK ADD  CONSTRAINT [FK_RoleParent] FOREIGN KEY([Parent])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[Role] CHECK CONSTRAINT [FK_RoleParent]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_UserManager] FOREIGN KEY([ManagerId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_UserManager]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([Id])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_Permission]
GO
ALTER TABLE [dbo].[UserPermission]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[UserPermission] CHECK CONSTRAINT [FK_UserPermission_User]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([Id])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[ApplicationUser]  WITH CHECK ADD  CONSTRAINT [CK_ApplicationUserPassword] CHECK  ((len([Password])>=(6)))
GO
ALTER TABLE [dbo].[ApplicationUser] CHECK CONSTRAINT [CK_ApplicationUserPassword]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [CK_UserPassword] CHECK  ((len([Password])>=(6)))
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [CK_UserPassword]
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
USE [master]
GO
ALTER DATABASE [UMS] SET  READ_WRITE 
GO


/****** Object:  User [UMSGateway]    Script Date: 12/30/2014 1:26:12 PM ******/
CREATE USER [UMSGateway] FOR LOGIN [UMSGateway]
GO
GRANT EXECUTE ON OBJECT::[dbo].[ApplicationAuthenticate] TO [UMSGateway]
GO
GRANT EXECUTE ON OBJECT::[dbo].[Authenticate] TO [UMSGateway]
GO