﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace Testing
{
    internal class Program
    {
        private static void Main1(string[] args)
        {
            var root = new Node {Id = 0, Name = "root"};
            var node1 = new Node {Id = 1, Name = "node 1", Parent = root};
            var node2 = new Node {Id = 2, Name = "node 2", Parent = root};
            root.Children.Add(node1);
            root.Children.Add(node2);

            var xml = ToXml(root);

            object tmp;
            var wrapperType = CreateWrapperType(root, out tmp);
            var serializer = new DataContractSerializer(wrapperType);
            using (Stream stream = new MemoryStream())
            {
                byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);
                stream.Write(data, 0, data.Length);
                stream.Position = 0;
                tmp = serializer.ReadObject(stream);
            }


            Console.WriteLine(xml);
            Console.Read();
        }

        public static string ToXml(object obj)
        {
            if (obj == null)
                return string.Empty;

            //var type = obj.GetType();
            //if(type.GetCustomAttributes(typeof (DataContractAttribute), false).Any())
            //{
            //    var serializer = new DataContractSerializer(type);
            //    var sb = new StringBuilder();
            //    var writer = XmlWriter.Create(sb, new XmlWriterSettings {Indent = true, Encoding = Encoding.UTF8});
            //    serializer.WriteObject(writer, obj);
            //    writer.Close();
            //    return sb.ToString();
            //}

            object output = null;
            var wrapperType = CreateWrapperType(obj, out output);
            var serializer1 = new DataContractSerializer(wrapperType);
            using (var stream = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(stream, new XmlWriterSettings {Indent = true, Encoding = new UTF8Encoding(false)}))
                {
                    serializer1.WriteObject(writer, output);
                }
                return Encoding.Default.GetString(stream.ToArray());
            }
        }

        public static Type CreateWrapperType(object obj, out object outObj)
        {
            var dataContractAttributeConstructorInfo = typeof (DataContractAttribute).GetConstructor(new Type[] {});
            var dataContractAttributeBuilder = new CustomAttributeBuilder(dataContractAttributeConstructorInfo,
                new object[] {});

            var dataMemberAttributeConstructorInfo = typeof(DataMemberAttribute).GetConstructor(new Type[] { });
            var dataMemberAttributeBuilder = new CustomAttributeBuilder(dataMemberAttributeConstructorInfo, new object[] { });

            var typeBuilder = CreateType(obj);
            typeBuilder.SetCustomAttribute(dataContractAttributeBuilder);

            foreach (var property in obj.GetType().GetProperties(BindingFlags.Instance|BindingFlags.Public))
            {
                CreateAutoProperty(typeBuilder, property.Name, property.PropertyType)
                    .SetCustomAttribute(dataMemberAttributeBuilder);
            }

            var returnType = typeBuilder.CreateType();
            outObj = Activator.CreateInstance(returnType);

            return returnType;
        }

        public static TypeBuilder CreateType(object obj)
        {
            var type = obj.GetType();
            var wrapperTypeName = type.FullName + "_Wrapper";
            var assemblyBuilder = Thread.GetDomain().DefineDynamicAssembly(new AssemblyName(wrapperTypeName), AssemblyBuilderAccess.Run);
            var modelBuilder = assemblyBuilder.DefineDynamicModule(wrapperTypeName);
            var typeBuilder = modelBuilder.DefineType(wrapperTypeName, TypeAttributes.Class | TypeAttributes.Public, null);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);

            return typeBuilder;
        }

        public static PropertyBuilder CreateAutoProperty(TypeBuilder typeBuilder, string propertyName, Type type)
        {
            var fieldBuiler = typeBuilder.DefineField("_" + propertyName, type, FieldAttributes.Private);
            var propertyBuilder = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, type, null);

            var getSetMethodAttribute = MethodAttributes.Public | MethodAttributes.SpecialName |
                                        MethodAttributes.HideBySig;

            var getMethodBuilder = typeBuilder.DefineMethod("get_" + propertyName, getSetMethodAttribute, type, Type.EmptyTypes);
            var getIL = getMethodBuilder.GetILGenerator();
            getIL.Emit(OpCodes.Ldarg_0);
            getIL.Emit(OpCodes.Ldfld, fieldBuiler);
            getIL.Emit(OpCodes.Ret);

            var setMethodBuilder = typeBuilder.DefineMethod("set_" + propertyName, getSetMethodAttribute, null, new Type[] {type});
            var setIL = setMethodBuilder.GetILGenerator();
            setIL.Emit(OpCodes.Ldarg_0);
            setIL.Emit(OpCodes.Ldarg_1);
            setIL.Emit(OpCodes.Stfld, fieldBuiler);
            setIL.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getMethodBuilder);
            propertyBuilder.SetSetMethod(setMethodBuilder);

            return propertyBuilder;
        }
    }
    public class Node
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Node Parent { get; set; }

        public List<Node> Children { get; set; } = new List<Node>();
    }
}
