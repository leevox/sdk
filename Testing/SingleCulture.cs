﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Testing
{
    //public class LocalizableTester
    //{
    //    public static void Main()
    //    {
    //        var db = new TestDb();

    //        var menu = new Menu {Position = 1 };
    //        menu.Name.Culture = "en-us";
    //        menu.Name.Value = "Home";
    //        menu.Description.Culture = "vi-vn";
    //        menu.Description.Value = "Day la trang home";
    //        db.Menu.Add(menu);

    //        var menu2 = new Menu { Position = 2 };
    //        menu2.Name.Culture = "vi-vn";
    //        menu2.Name.Value = "San pham";
    //        menu2.Description.Culture = "en-us";
    //        menu2.Description.Value = "This is product page";
    //        db.Menu.Add(menu2);

    //        var prod = new Product { Price = 1000 };
    //        prod.ProductName.Culture = "en-us";
    //        prod.ProductName.Value = "Product 1";
    //        prod.ProductDescription.Culture = "vi-vn";
    //        prod.ProductDescription.Value = "Day la san pham 1";
    //        db.Products.Add(prod);

    //        var prod2 = new Product {Price = 200 };
    //        prod2.ProductName.Culture = "vi-vn";
    //        prod2.ProductName.Value = "san pham 2";
    //        prod2.ProductDescription.Culture = "en-us";
    //        prod2.ProductDescription.Value = "This is product 2";
    //        db.Products.Add(prod2);

    //        db.SaveChanges();

    //        var x = db.Menu.ToList();
    //        var y = db.Products.ToList();
    //        var i = 1;


    //        Console.WriteLine("ok");
    //        Console.Read();
    //    }
    //}

    //public class TestDb : DbContext
    //{
    //    public DbSet<Menu> Menu { get; set; }
    //    public DbSet<Product> Products { get; set; }

    //    public TestDb()
    //    {
    //        //Database.SetInitializer<TestDb>(new DropCreateDatabaseAlways<TestDb>());
    //    }

    //    protected override void OnModelCreating(DbModelBuilder model)
    //    {
    //        model.Entity<Menu>().Map(m =>
    //        {
    //            m.Properties(t=>new {t.Id, t.Name,t.Description});
    //            m.ToTable("MenuRes");
    //        })
    //        .Map(m =>
    //        {
    //            m.Properties(t => new { t.Id, t.Position });
    //            m.ToTable("Menu");
    //        });

    //        model.Entity<Product>().Map(m =>
    //        {
    //            m.Properties(t => new { t.Id, t.ProductName, t.ProductDescription});
    //            m.ToTable("ProductRes");
    //        })
    //        .Map(m =>
    //        {
    //            m.Properties(t => new { t.Id, t.Price });
    //            m.ToTable("Product");
    //        });

    //        model.Entity<Menu>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
    //        model.Entity<Product>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
    //    }
    //}

    //#region model
    
    //public class Menu
    //{
    //    public int Id { get; set; }
    //    public int Position { get; set; }
    //    public LocalizableDic Name { get; set; } = new LocalizableDic();
    //    public LocalizableDic Description { get; set; } = new LocalizableDic();
    //}
    
    //public class Product
    //{
    //    public int Id { get; set; }
    //    public int Price { get; set; }
    //    public LocalizableDic ProductName { get; set; } = new LocalizableDic();
    //    public LocalizableDic ProductDescription { get; set; } =new LocalizableDic();
    //}
    
    //[ComplexType]
    //public class LocalizableDic
    //{
    //    public string Culture { get; set; }
    //    public string Value { get; set; }
    //}

    //#endregion
}
