﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Testing
{
    public class LocalizableTester
    {
        public static void Main()
        {
            var db = new TestDb();

            var menu = new Menu { Position = 1 };
            menu.Name.Add(new MenuLocalizableItem { Culture = "en-us", Value = "Home", PropertyName = "Name"});
            menu.Name.Add(new MenuLocalizableItem { Culture = "vi-vn", Value = "Trang chu", PropertyName = "Name" });
            menu.Description.Add(new MenuLocalizableItem { Culture = "en-us", Value = "This is home", PropertyName = "Description" });
            menu.Description.Add(new MenuLocalizableItem { Culture = "vi-vn", Value = "Day la trang chu", PropertyName = "Description" });
            db.Menu.Add(menu);

            var menu2 = new Menu { Position = 2 };
            menu2.Name.Add(new MenuLocalizableItem { Culture = "en-us", Value = "Categories", PropertyName = "Name" });
            menu2.Name.Add(new MenuLocalizableItem { Culture = "vi-vn", Value = "Danh muc", PropertyName = "Name" });
            menu2.Description.Add(new MenuLocalizableItem { Culture = "en-us", Value = "This is categories", PropertyName = "Description" });
            menu2.Description.Add(new MenuLocalizableItem { Culture = "vi-vn", Value = "Day la danh muc", PropertyName = "Description" });
            db.Menu.Add(menu2);

            var prod = new Product { Price = 1000 };
            prod.ProductName.Add(new ProductLocalizableItem { Culture = "en-us", Value = "Product 1", PropertyName = "ProductName" });
            prod.ProductName.Add(new ProductLocalizableItem { Culture = "vi-vn", Value = "San pham 1", PropertyName = "ProductName" });
            prod.ProductDescription.Add(new ProductLocalizableItem { Culture = "en-us", Value = "This is product 1", PropertyName = "ProductDescription" });
            prod.ProductDescription.Add(new ProductLocalizableItem { Culture = "vi-vn", Value = "Day la san pham 1", PropertyName = "ProductDescription" });
            db.Products.Add(prod);

            var prod2 = new Product { Price = 200 };
            prod2.ProductName.Add(new ProductLocalizableItem { Culture = "en-us", Value = "Product 2", PropertyName = "ProductName" });
            prod2.ProductName.Add(new ProductLocalizableItem { Culture = "vi-vn", Value = "San pham 2", PropertyName = "ProductName" });
            prod2.ProductDescription.Add(new ProductLocalizableItem { Culture = "en-us", Value = "This is product 2", PropertyName = "ProductDescription" });
            prod2.ProductDescription.Add(new ProductLocalizableItem { Culture = "vi-vn", Value = "Day la san pham 2", PropertyName = "ProductDescription" });
            db.Products.Add(prod2);

            
            db.SaveChanges();

            var x = db.Menu.ToList();
            var y = db.Products.ToList();
            var i = 1;


            Console.WriteLine("ok");
            Console.Read();
        }
    }

    public class TestDb : DbContext
    {
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Product> Products { get; set; }

        public TestDb()
        {
            //Database.SetInitializer<TestDb>(new DropCreateDatabaseAlways<TestDb>());
        }

        protected override void OnModelCreating(DbModelBuilder model)
        {
            //model.Entity<Menu>().Map(m =>
            //{
            //    m.Properties(t => new { t.Id, t.Name, t.Description });
            //    m.ToTable("MenuRes");
            //})
            //.Map(m =>
            //{
            //    m.Properties(t => new { t.Id, t.Position });
            //    m.ToTable("Menu");
            //});

            //model.Entity<Product>().Map(m =>
            //{
            //    m.Properties(t => new { t.Id, t.ProductName, t.ProductDescription });
            //    m.ToTable("ProductRes");
            //})
            //.Map(m =>
            //{
            //    m.Properties(t => new { t.Id, t.Price });
            //    m.ToTable("Product");
            //});
            //model.Entity<Menu>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //model.Entity<Product>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

        }
    }

    #region model

    public class Menu
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public virtual Localizable<MenuLocalizableItem> Name { get; set; } = new Localizable<MenuLocalizableItem>();
        public virtual Localizable<MenuLocalizableItem> Description { get; set; } = new Localizable<MenuLocalizableItem>();
    }

    public class Product
    {
        public int Id { get; set; }
        public int Price { get; set; }
        public virtual Localizable<ProductLocalizableItem> ProductName { get; set; } = new Localizable<ProductLocalizableItem>();
        public virtual Localizable<ProductLocalizableItem> ProductDescription { get; set; } = new Localizable<ProductLocalizableItem>();
    }

    public class LocalizableItem
    {
        public int Id { get; set; }
        public string Culture { get; set; }
        public string Value { get; set; }
        public string PropertyName { get; set; }
    }

    public class MenuLocalizableItem : LocalizableItem
    {
    }

    public class ProductLocalizableItem : LocalizableItem
    {
    }

    public class Localizable<T> : List<T>
    {
        
    }

    #endregion
}
