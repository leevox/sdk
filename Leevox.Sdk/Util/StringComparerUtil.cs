﻿using System;

namespace Leevox.Sdk.Util
{
    public static class StringComparerUtil
    {
        /// <summary>
        /// Ordinal string comparer ignore leading + trailing white spaces.
        /// </summary>
        public static StringComparer OrdinalIgnoreSpaces => OrdinalIgnoreSpacesObj.Value;

        /// <summary>
        /// Ordinal string comparer ignore case and leading + trailing white spaces.
        /// </summary>
        public static StringComparer OrdinalIgnoreCaseAndSpaces => OrdinalIgnoreCaseAndSpacesObj.Value;

        /// <summary>
        /// CurrentCulture string comparer ignore leading + trailing white spaces.
        /// </summary>
        public static StringComparer CurrentCultureIgnoreSpaces => CurrentCultureIgnoreSpacesObj.Value;

        /// <summary>
        /// CurrentCulture string comparer ignore case and leading + trailing white spaces.
        /// </summary>
        public static StringComparer CurrentCultureIgnoreCaseAndSpaces => CurrentCultureIgnoreCaseAndSpacesObj.Value;

        /// <summary>
        /// InvariantCulture string comparer ignore leading + trailing white spaces.
        /// </summary>
        public static StringComparer InvariantCultureIgnoreSpaces => InvariantCultureIgnoreSpacesObj.Value;

        /// <summary>
        /// InvariantCulture string comparer ignore case and leading + trailing white spaces.
        /// </summary>
        public static StringComparer InvariantCultureIgnoreCaseAndSpaces => InvariantCultureIgnoreCaseAndSpacesObj.Value;

        private static readonly Lazy<OrdinalIgnoreSpacesComparer> OrdinalIgnoreSpacesObj = new Lazy<OrdinalIgnoreSpacesComparer>();
        private static readonly Lazy<OrdinalIgnoreCaseAndSpacesComparer> OrdinalIgnoreCaseAndSpacesObj = new Lazy<OrdinalIgnoreCaseAndSpacesComparer>();
        private static readonly Lazy<CurrentCultureIgnoreSpacesComparer> CurrentCultureIgnoreSpacesObj = new Lazy<CurrentCultureIgnoreSpacesComparer>();
        private static readonly Lazy<CurrentCultureIgnoreCaseAndSpacesComparer> CurrentCultureIgnoreCaseAndSpacesObj = new Lazy<CurrentCultureIgnoreCaseAndSpacesComparer>();
        private static readonly Lazy<InvariantCultureIgnoreSpacesComparer> InvariantCultureIgnoreSpacesObj = new Lazy<InvariantCultureIgnoreSpacesComparer>();
        private static readonly Lazy<InvariantCultureIgnoreCaseAndSpacesComparer> InvariantCultureIgnoreCaseAndSpacesObj = new Lazy<InvariantCultureIgnoreCaseAndSpacesComparer>();

        private static int Compare(StringComparer comparer, string x, string y)
        {
            return comparer.Compare(x.TrimOrEmpty(), y.TrimOrEmpty());
        }

        private static bool Equals(StringComparer comparer, string x, string y)
        {
            return comparer.Equals(x.TrimOrEmpty(), y.TrimOrEmpty());
        }

        private static int GetHashCode(StringComparer comparer, string obj)
        {
            return comparer.GetHashCode(obj.TrimOrEmpty());
        }

        #region comparer classes

        internal class OrdinalIgnoreSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(Ordinal, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(Ordinal, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(Ordinal, obj);
            }
        }

        internal class OrdinalIgnoreCaseAndSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(OrdinalIgnoreCase, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(OrdinalIgnoreCase, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(OrdinalIgnoreCase, obj);
            }
        }

        internal class CurrentCultureIgnoreSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(CurrentCulture, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(CurrentCulture, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(CurrentCulture, obj);
            }
        }

        internal class CurrentCultureIgnoreCaseAndSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(CurrentCultureIgnoreCase, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(CurrentCultureIgnoreCase, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(CurrentCultureIgnoreCase, obj);
            }
        }

        internal class InvariantCultureIgnoreSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(InvariantCulture, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(InvariantCulture, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(InvariantCulture, obj);
            }
        }

        internal class InvariantCultureIgnoreCaseAndSpacesComparer : StringComparer
        {
            public override int Compare(string x, string y)
            {
                return StringComparerUtil.Compare(InvariantCultureIgnoreCase, x, y);
            }

            public override bool Equals(string x, string y)
            {
                return StringComparerUtil.Equals(InvariantCultureIgnoreCase, x, y);
            }

            public override int GetHashCode(string obj)
            {
                return StringComparerUtil.GetHashCode(InvariantCultureIgnoreCase, obj);
            }
        }

        #endregion
    }
}
