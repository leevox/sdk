﻿using System;
using System.Security.Cryptography;

namespace Leevox.Sdk.Util
{
    [Flags]
    public enum RandomNumberOption
    {
        /// <summary>
        /// The random number is greater than or equal MinValue, this will overwrite ExcludedMinValue option
        /// </summary>
        IncludedMinValue,

        /// <summary>
        /// The random number is less than or equal MaxValue, this will overwrite ExcludedMaxValue option
        /// </summary>
        IncludedMaxValue,

        /// <summary>
        /// The random number is always greater than MinValue
        /// </summary>
        ExcludedMinValue,

        /// <summary>
        /// The random number is always less than MaxValue
        /// </summary>
        ExcludedMaxValue
    }

    public static class RandomNumber
    {
        private const int SizeInt = sizeof(int);
        private const int SizeLong = sizeof(long);

        private static readonly Lazy<RNGCryptoServiceProvider> Generator = new Lazy<RNGCryptoServiceProvider>();

        #region double

        /// <summary>
        /// Random a double number in range [0, 1] (included 0 and 1)
        /// </summary>
        public static double RandomDouble()
        {
            return RandomDouble(RandomNumberOption.IncludedMinValue | RandomNumberOption.IncludedMaxValue);
        }

        /// <summary>
        /// Random a double number in range [0, 1] with option
        /// </summary>
        public static double RandomDouble(RandomNumberOption option)
        {
            double result;
            double maxValue = ulong.MaxValue;
            var bytes = new byte[SizeLong];
            do
            {
                Generator.Value.GetBytes(bytes);
                result = BitConverter.ToUInt64(bytes, 0)/maxValue;
            } while (
                double.IsNaN(result)
                || double.IsInfinity(result)
                || (result == 0 && option.HasFlag(RandomNumberOption.ExcludedMinValue))
                || (result == 1 && option.HasFlag(RandomNumberOption.ExcludedMaxValue))
            );
            return result;
        }

        #endregion

        #region float

        /// <summary>
        /// Random a float number in range [0, 1] (included 0 and 1)
        /// </summary>
        public static float RandomFloat()
        {
            return RandomFloat(RandomNumberOption.IncludedMinValue | RandomNumberOption.IncludedMaxValue);
        }

        /// <summary>
        /// Random a float number in range [0, 1] with option
        /// </summary>
        public static float RandomFloat(RandomNumberOption option)
        {
            float result;
            float maxValue = ulong.MaxValue;
            var bytes = new byte[SizeLong];
            do
            {
                Generator.Value.GetBytes(bytes);
                result = BitConverter.ToUInt64(bytes, 0) / maxValue;
            } while (
                float.IsNaN(result)
                || float.IsInfinity(result)
                || (result == 0 && option.HasFlag(RandomNumberOption.ExcludedMinValue))
                || (result == 1 && option.HasFlag(RandomNumberOption.ExcludedMaxValue))
            );
            return result;
        }

        #endregion

        #region int

        public static int RandomInt()
        {
            return RandomInt(RandomNumberOption.IncludedMinValue | RandomNumberOption.IncludedMaxValue);
        }

        public static int RandomInt(RandomNumberOption option)
        {
            int result;
            byte[] buffer = new byte[SizeInt];
            do
            {
                Generator.Value.GetBytes(buffer);
                result = BitConverter.ToInt32(buffer, 0);

            }
            while (
                (result == int.MinValue && option.HasFlag(RandomNumberOption.ExcludedMinValue))
                || (result == int.MaxValue && option.HasFlag(RandomNumberOption.ExcludedMaxValue))
            );
            return result;
        }

        public static int RandomInt(int min)
        {
            return RandomInt(min, int.MaxValue, RandomNumberOption.IncludedMinValue | RandomNumberOption.IncludedMaxValue);
        }

        public static int RandomInt(int min, RandomNumberOption option)
        {
            return RandomInt(min, int.MaxValue, option);
        }

        public static int RandomInt(int min, int max)
        {
            return RandomInt(min, max, RandomNumberOption.IncludedMinValue | RandomNumberOption.IncludedMaxValue);
        }

        public static int RandomInt(int min, int max, RandomNumberOption option)
        {

            var rnd = RandomDouble(option);
            var diff = (long)max - min;
            return (int)(min + diff*rnd);
        }

        #endregion
    }
}
