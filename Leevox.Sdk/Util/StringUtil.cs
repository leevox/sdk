﻿using System;
using System.Linq;

namespace Leevox.Sdk.Util
{
    public static class StringUtil
    {
        /// <summary>
        /// Check if 2 strings are ordinal equal.
        /// </summary>
        public static bool IsOrdinalEqual(this string str, string text)
        {
            return string.Compare(str, text, StringComparison.Ordinal) == 0;
        }

        /// <summary>
        /// Check if 2 strings are ordinal equal, ignore case.
        /// </summary>
        public static bool IsOrdinalEqualIgnoreCase(this string str, string text)
        {
            return string.Compare(str, text, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Check if 2 strings are ordinal equal, ignore leading + trailing white spaces.
        /// </summary>
        public static bool IsOrdinalEqualIgnoreSpaces(this string str, string text)
        {
            return string.Compare(str.TrimOrEmpty(), text.TrimOrEmpty(), StringComparison.Ordinal) == 0;
        }

        /// <summary>
        /// Check if 2 strings are ordinal equal, ignore case and leading + trailing white spaces.
        /// </summary>
        public static bool IsOrdinalEqualIgnoreCaseAndSpaces(this string str, string text)
        {
            return string.Compare(str.TrimOrEmpty(), text.TrimOrEmpty(), StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Shortcut to string.Trim(text)
        /// <para>Return string.Empty if input string is null</para>
        /// </summary>
        public static string TrimOrEmpty(this string text)
        {
            return TrimOrEmpty(text, null);
        }

        /// <summary>
        /// Shortcut to string.Trim(text, trimChars)
        /// <para>Return string.Empty if input string is null</para>
        /// </summary>
        public static string TrimOrEmpty(this string text, params char[] trimChars)
        {
            return text?.Trim(trimChars) ?? string.Empty;
        }

        /// <summary>
        /// Shortcut to string.IsNullOrWhiteSpace(text)
        /// </summary>
        public static bool IsNullOrWhiteSpace(this string text)
        {
            return string.IsNullOrWhiteSpace(text);
        }

        /// <summary>
        /// Shortcut to string.Format(objs)
        /// <para>Return string.Empty if input string is null</para>
        /// </summary>
        public static string Format(this string text, params object[] objs)
        {
            return text == null ? string.Empty : string.Format(text, objs);
        }

        /// <summary>
        /// Repeat a string.
        /// </summary>
        public static string Repeat(this string text, int count)
        {
            if (text == null || count < 1)
                return string.Empty;

            if (count == 1)
                return text;

            if (text.Length == 1)
                return Repeat(text[0], count);

            return string.Concat(Enumerable.Repeat(text, count));
        }

        /// <summary>
        /// Repeat a character.
        /// </summary>
        public static string Repeat(this char c, int count)
        {
            return new string(c, count);
        }

        /// <summary>
        /// Return value if text is null or white space.
        /// </summary>
        public static string ValueIfNull(this string text, string value)
        {
            return string.IsNullOrWhiteSpace(text) ? value : text;
        }
    }
}
