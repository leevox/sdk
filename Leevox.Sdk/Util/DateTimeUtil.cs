﻿using System;

namespace Leevox.Sdk.Util
{
    public static class DateTimeUtil
    {
        /// <summary>
        /// Get DateTime from current time zone to UTC+0
        /// </summary>
        public static DateTime UtcDateTime(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime();
        }

        /// <summary>
        /// Get Date from current time zone to UTC+0
        /// </summary>
        public static DateTime UtcDate(this DateTime dateTime)
        {
            return UtcDateTime(dateTime).Date;
        }

        /// <summary>
        /// Get DateTime from UTC+0 to current time zone
        /// </summary>
        public static DateTime CurrentDateTime(this DateTime utcDateTime)
        {
            return TimeZone.CurrentTimeZone.ToLocalTime(utcDateTime);
        }

        /// <summary>
        /// Get Date from UTC+0 to current time zone
        /// </summary>
        public static DateTime CurrentDate(this DateTime utcDateTime)
        {
            return CurrentDateTime(utcDateTime).Date;
        }
    }
}
