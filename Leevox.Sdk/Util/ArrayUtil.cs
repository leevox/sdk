﻿using System.Collections.Generic;
using System.Linq;

namespace Leevox.Sdk.Util
{
    public static class ArrayUtil
    {
        public static string ToHexString(this IEnumerable<byte> bytes)
        {
            return ToHexString(bytes, string.Empty);
        }

        public static string ToHexString(this IEnumerable<byte> bytes, string seperator)
        {
            if (bytes == null)
                return string.Empty;

            return string.Join(seperator, bytes.Select(x => x.ToString("X2")));
        }
    }
}
