﻿
namespace Leevox.Sdk.Util
{
    public static class ParsingUtil
    {
        /// <summary>
        /// Try pare to bool, return null if cannot parse
        /// </summary>
        public static bool? ToBool(this string value)
        {
            bool ret;
            if (bool.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to bool, return defaultValue if cannot parse
        /// </summary>
        public static bool ToBool(this string value, bool defaultValue)
        {
            return ToBool(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to byte, return null if cannot parse
        /// </summary>
        public static byte? ToByte(this string value)
        {
            byte ret;
            if (byte.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to byte, return defaultValue if cannot parse
        /// </summary>
        public static byte ToByte(this string value, byte defaultValue)
        {
            return ToByte(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to sbyte, return null if cannot parse
        /// </summary>
        public static sbyte? ToSByte(this string value)
        {
            sbyte ret;
            if (sbyte.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to sbyte, return defaultValue if cannot parse
        /// </summary>
        public static sbyte ToSByte(this string value, sbyte defaultValue)
        {
            return ToSByte(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to short, return null if cannot parse
        /// </summary>
        public static short? ToShort(this string value)
        {
            short ret;
            if (short.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to short, return defaultValue if cannot parse
        /// </summary>
        public static short ToShort(this string value, short defaultValue)
        {
            return ToShort(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to ushort, return null if cannot parse
        /// </summary>
        public static ushort? ToUShort(this string value)
        {
            ushort ret;
            if (ushort.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to ushort, return defaultValue if cannot parse
        /// </summary>
        public static ushort ToUShort(this string value, ushort defaultValue)
        {
            return ToUShort(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to int, return null if cannot parse
        /// </summary>
        public static int? ToInt(this string value)
        {
            int ret;
            if (int.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to int, return defaultValue if cannot parse
        /// </summary>
        public static int ToInt(this string value, int defaultValue)
        {
            return ToInt(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to uint, return null if cannot parse
        /// </summary>
        public static uint? ToUInt(this string value)
        {
            uint ret;
            if (uint.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to uint, return defaultValue if cannot parse
        /// </summary>
        public static uint ToToUInt(this string value, uint defaultValue)
        {
            return ToUInt(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to long, return null if cannot parse
        /// </summary>
        public static long? ToLong(this string value)
        {
            long ret;
            if (long.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to long, return defaultValue if cannot parse
        /// </summary>
        public static long ToLong(this string value, long defaultValue)
        {
            return ToLong(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to ulong, return null if cannot parse
        /// </summary>
        public static ulong? ToULong(this string value)
        {
            ulong ret;
            if (ulong.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to ulong, return defaultValue if cannot parse
        /// </summary>
        public static ulong ToULong(this string value, ulong defaultValue)
        {
            return ToULong(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to float, return null if cannot parse
        /// </summary>
        public static float? ToFloat(this string value)
        {
            float ret;
            if (float.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to float, return defaultValue if cannot parse
        /// </summary>
        public static float ToFloat(this string value, float defaultValue)
        {
            return ToFloat(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to double, return null if cannot parse
        /// </summary>
        public static double? ToDouble(this string value)
        {
            double ret;
            if (double.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to double, return defaultValue if cannot parse
        /// </summary>
        public static double ToDouble(this string value, double defaultValue)
        {
            return ToDouble(value) ?? defaultValue;
        }

        /// <summary>
        /// Try pare to decimal, return null if cannot parse
        /// </summary>
        public static decimal? ToDecimal(this string value)
        {
            decimal ret;
            if (decimal.TryParse(value, out ret))
                return ret;
            return null;
        }

        /// <summary>
        /// Try pare to decimal, return defaultValue if cannot parse
        /// </summary>
        public static decimal ToDecimal(this string value, decimal defaultValue)
        {
            return ToDecimal(value) ?? defaultValue;
        }
    }
}
