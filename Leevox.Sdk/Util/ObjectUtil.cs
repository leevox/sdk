﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using Leevox.Sdk.Hash;

namespace Leevox.Sdk.Util
{
    public static class ObjectUtil
    {
        private static readonly Lazy<BinaryFormatter> BinaryFormatterHelper = new Lazy<BinaryFormatter>();
        private static readonly Lazy<Fnv1A> Fnv1AHelper = new Lazy<Fnv1A>();
        private static readonly Lazy<Murmur3> Murmur3Helper = new Lazy<Murmur3>();
        private static readonly Lazy<MD5> Md5Helper = new Lazy<MD5>(MD5.Create);
        private static readonly Lazy<SHA1> Sha1Helper = new Lazy<SHA1>(SHA1.Create);
        private static readonly Lazy<SHA256> Sha256Helper = new Lazy<SHA256>(SHA256.Create);

        /// <summary>
        /// Get byte array represent the object
        /// </summary>
        public static byte[] GetBytes(this object obj)
        {
            if (obj == null)
                return null;

            if (obj is IEnumerable<byte>)
                return obj as byte[];

            var s = obj as string;
            if (s != null)
                return Encoding.UTF8.GetBytes(s);

            var stream = obj as Stream;
            if (stream != null)
            {
                var ret = new byte[stream.Length];
                stream.Read(ret, 0, ret.Length);
                return ret;
            }

            using (var ms = new MemoryStream())
            {
                BinaryFormatterHelper.Value.Serialize(ms, obj);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Caculate FNV-1a hash of the object
        /// </summary>
        public static ulong Fnv1A(this object obj)
        {
            if (obj == null)
                return 0L;

            return Fnv1AHelper.Value.ComputeHash(obj.GetBytes());
        }

        /// <summary>
        /// Caculate MurMur3_x64_128 hash of the object
        /// </summary>
        public static byte[] Murmur3(this object obj)
        {
            if (obj == null)
                return null;

            var stream = obj as Stream;
            if (stream != null)
                return Murmur3Helper.Value.Murmur3_x64_128(stream);

            return Murmur3Helper.Value.Murmur3_x64_128(obj.GetBytes());
        }

        /// <summary>
        /// Caculate MD5 hash of the object
        /// </summary>
        public static byte[] Md5(this object obj)
        {
            if (obj == null)
                return null;

            var stream = obj as Stream;
            if (stream != null)
                return Md5Helper.Value.ComputeHash(stream);

            return Md5Helper.Value.ComputeHash(obj.GetBytes());
        }

        /// <summary>
        /// Caculate SHA1 hash of the object
        /// </summary>
        public static byte[] Sha1(this object obj)
        {
            if (obj == null)
                return null;

            var stream = obj as Stream;
            if (stream != null)
                return Sha1Helper.Value.ComputeHash(stream);

            return Sha1Helper.Value.ComputeHash(obj.GetBytes());
        }

        /// <summary>
        /// Caculate SHA-256 hash of the object
        /// </summary>
        public static byte[] Sha256(this object obj)
        {
            if (obj == null)
                return null;

            var stream = obj as Stream;
            if (stream != null)
                return Sha256Helper.Value.ComputeHash(stream);

            return Sha256Helper.Value.ComputeHash(obj.GetBytes());
        }
    }
}
