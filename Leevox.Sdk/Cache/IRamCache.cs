using System;
using System.Collections.Generic;

namespace Leevox.Sdk.Cache
{
    public interface IRamCache<TKey, TEntity>
    {
        /// <summary>
        /// Auto load entity if it does not existed in the cache.
        /// </summary>
        bool AutoLoadIfNotExisted { get; set; }

        /// <summary>
        /// Default life time when add an entity into cache.
        /// </summary>
        TimeSpan DefaultLifeTime { get; set; }

        /// <summary>
        /// Interval for refreshing the cache.
        /// </summary>
        TimeSpan RefreshInterval { get; set; }

        /// <summary>
        /// Function delegate for loading an entity.
        /// </summary>
        Func<TKey, TEntity> LoadEntity { get; set; }

        /// <summary>
        /// Function delegate for loading entities.
        /// </summary>
        Func<IEnumerable<TKey>, IEnumerable<KeyValuePair<TKey, TEntity>>> LoadEntities { get; set; }

        /// <summary>
        /// Action when entities was expired and removed from the cache.
        /// </summary>
        Action<IEnumerable<TKey>> OnEntityExpired { get; set; }

        /// <summary>
        /// Clear the cache
        /// </summary>
        void Clear();

        /// <summary>
        /// Check if the cache contains a key.
        /// </summary>
        /// <param name="key">Key to check</param>
        bool ContainsKey(TKey key);

        /// <summary>
        /// Add an entity into cache with default life time. Throw exception if key is already existed.
        /// </summary>
        TEntity Add(TKey key, TEntity entity);

        /// <summary>
        /// Add an entity into cache with specific life time. Throw exception if key is already existed.
        /// </summary>
        TEntity Add(TKey key, TEntity entity, TimeSpan lifeTime);

        /// <summary>
        /// Add an entity into cache. If key is existed, replace current entity by new entity.
        /// </summary>
        TEntity AddOrReplace(TKey key, TEntity entity);

        /// <summary>
        /// Add an entity into cache with specific life time. If key is existed, replace current entity by new entity.
        /// </summary>
        TEntity AddOrReplace(TKey key, TEntity entity, TimeSpan lifeTime);

        /// <summary>
        /// Remove an entity from the cache.
        /// </summary>
        void Remove(TKey key);

        /// <summary>
        /// Add entities into cache with default life time. Throw exception if any key is already existed.
        /// </summary>
        void AddRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities);

        /// <summary>
        /// Add entities into cache with specific life time. Throw exception if any key is already existed.
        /// </summary>
        void AddRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities, TimeSpan lifeTime);

        /// <summary>
        /// Add entities into cache. If key is existed, replace current entity by new entity.
        /// </summary>
        void AddOrReplaceRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities);

        /// <summary>
        /// Add entities into cache with specific life time. If key is existed, replace current entity by new entity.
        /// </summary>
        void AddOrReplaceRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities, TimeSpan lifeTime);

        /// <summary>
        /// Remove entities from the cache.
        /// </summary>
        void Remove(IEnumerable<TKey> keys);

        /// <summary>
        /// Get entity from cache. Return null if key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        TEntity Get(TKey key);

        /// <summary>
        /// Get all entities from the cache.
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

            /// <summary>
        /// Get entity from cache. Load entity if key does not exist.
        /// </summary>
        TEntity GetOrLoad(TKey key);

        /// <summary>
        /// Get entities from cache. Load entities whose key does not exist.
        /// </summary>
        IEnumerable<TEntity> GetOrLoad(params TKey[] keys);

        /// <summary>
        /// Get entities from cache. Load entities whose key does not exist.
        /// </summary>
        IEnumerable<TEntity> GetOrLoad(IEnumerable<TKey> keys);

        TEntity this[TKey key] { get; set; }
    }
}