﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Leevox.Sdk.Cache
{
    public class RamCache<TKey, TEntity> : IRamCache<TKey, TEntity>, IDisposable
    {
        internal class EntityWrapper
        {
            internal DateTime ExpiredTime { get; set; }
            internal TEntity Entity { get; set; }

            internal EntityWrapper(TEntity entity, DateTime expiredTime)
            {
                Entity = entity;
                ExpiredTime = expiredTime;
            }
        }

        private bool _isDisposed;
        private Thread _garbageCollectorThread;
        private readonly object _lock = new object();

        internal Dictionary<TKey, EntityWrapper> Storage { get; set; }

        /// <summary>
        /// Auto load entity if it does not existed in the cache.
        /// </summary>
        public bool AutoLoadIfNotExisted { get; set; } = true;

        /// <summary>
        /// Auto load entity if it does not existed in the cache.
        /// </summary>
        public bool AutoReloadIfExpired { get; set; } = false;

        /// <summary>
        /// Default life time when add an entity into cache.
        /// </summary>
        public TimeSpan DefaultLifeTime { get; set; } = TimeSpan.FromMinutes(10);

        /// <summary>
        /// Interval for refreshing the cache.
        /// </summary>
        public TimeSpan RefreshInterval { get; set; } = TimeSpan.FromSeconds(10);

        /// <summary>
        /// Function delegate for loading an entity.
        /// </summary>
        public Func<TKey, TEntity> LoadEntity { get; set; }

        /// <summary>
        /// Function delegate for loading entities.
        /// </summary>
        public Func<IEnumerable<TKey>, IEnumerable<KeyValuePair<TKey, TEntity>>> LoadEntities { get; set; }

        /// <summary>
        /// Action when entities was expired and removed from the cache.
        /// </summary>
        public Action<IEnumerable<TKey>> OnEntityExpired { get; set; }

        public RamCache(Func<TKey, TEntity> loadEntity, Func<IEnumerable<TKey>, IEnumerable<KeyValuePair<TKey, TEntity>>> loadEntities)
        {
            LoadEntities = loadEntities;
            LoadEntity = loadEntity;
            Storage = new Dictionary<TKey, EntityWrapper>();

            (_garbageCollectorThread = new Thread(GarbageCollectorThreadStart)).Start();
        }

        public RamCache(Func<TKey, TEntity> loadEntity, Func<IEnumerable<TKey>, IEnumerable<KeyValuePair<TKey, TEntity>>> loadEntities, IEqualityComparer<TKey> comparer)
        {
            LoadEntities = loadEntities;
            LoadEntity = loadEntity;
            Storage = new Dictionary<TKey, EntityWrapper>(comparer);

            (_garbageCollectorThread = new Thread(GarbageCollectorThreadStart)).Start();
        }

        protected void GarbageCollectorThreadStart()
        {
            while (!_isDisposed)
            {
                if (Storage.Count > 0)
                {
                    DateTime now = DateTime.Now;
                    var expiredList = new List<TKey>();
                    lock (_lock)
                    {
                        foreach (var key in Storage.Keys.ToList())
                            if (Storage[key].ExpiredTime <= now)
                            {
                                expiredList.Add(key);
                                Storage.Remove(key);
                            }
                    }
                    if (OnEntityExpired != null && expiredList.Count > 0)
                        OnEntityExpired(expiredList);

                    if (AutoReloadIfExpired && expiredList.Count > 0)
                        GetOrLoad(expiredList);
                }
                Thread.Sleep(RefreshInterval);
            }
        }

        /// <summary>
        /// Clear the cache
        /// </summary>
        public void Clear()
        {
            Storage.Clear();
        }

        /// <summary>
        /// Check if the cache contains a key.
        /// </summary>
        /// <param name="key">Key to check</param>
        public bool ContainsKey(TKey key)
        {
            if (key == null)
                return false;
            return Storage.ContainsKey(key);
        }

        /// <summary>
        /// Add an entity into cache with default life time. Throw exception if key is already existed.
        /// </summary>
        public TEntity Add(TKey key, TEntity entity)
        {
            return Add(key, entity, DefaultLifeTime);
        }

        /// <summary>
        /// Add an entity into cache with specific life time. Throw exception if key is already existed.
        /// </summary>
        public TEntity Add(TKey key, TEntity entity, TimeSpan lifeTime)
        {
            if (key == null)
                throw new ArgumentException(nameof(key));

            lock (_lock)
            {
                Storage.Add(key, new EntityWrapper(entity, DateTime.Now.Add(lifeTime)));
                return entity;
            }
        }

        /// <summary>
        /// Remove an entity from the cache.
        /// </summary>
        public void Remove(TKey key)
        {
            if (key == null)
                throw new ArgumentException(nameof(key));

            lock (_lock)
            {
                Storage.Remove(key);
            }
        }

        /// <summary>
        /// Add an entity into cache. If key is existed, replace current entity by new entity.
        /// </summary>
        public TEntity AddOrReplace(TKey key, TEntity entity)
        {
            return AddOrReplace(key, entity, DefaultLifeTime);
        }

        /// <summary>
        /// Add an entity into cache with specific life time. If key is existed, replace current entity by new entity.
        /// </summary>
        public TEntity AddOrReplace(TKey key, TEntity entity, TimeSpan lifeTime)
        {
            if (key == null)
                throw new ArgumentException(nameof(key));

            lock (_lock)
            {
                if (Storage.ContainsKey(key))
                    Storage[key] = new EntityWrapper(entity, DateTime.Now.Add(lifeTime));
                else
                    Storage.Add(key, new EntityWrapper(entity, DateTime.Now.Add(lifeTime)));
                return entity;
            }
        }

        /// <summary>
        /// Add entities into cache with default life time. Throw exception if any key is already existed.
        /// </summary>
        public void AddRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities)
        {
            AddRange(entities, DefaultLifeTime);
        }

        /// <summary>
        /// Add entities into cache with specific life time. Throw exception if any key is already existed.
        /// </summary>
        public void AddRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities, TimeSpan lifeTime)
        {
            if (entities == null)
                throw new ArgumentException(nameof(entities));

            lock (_lock)
            {
                foreach (var kvp in entities)
                {
                    Storage.Add(kvp.Key, new EntityWrapper(kvp.Value, DateTime.Now.Add(lifeTime)));
                }
            }
        }

        /// <summary>
        /// Add entities into cache. If key is existed, replace current entity by new entity.
        /// </summary>
        public void AddOrReplaceRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities)
        {
            AddOrReplaceRange(entities, DefaultLifeTime);
        }

        /// <summary>
        /// Add entities into cache with specific life time. If key is existed, replace current entity by new entity.
        /// </summary>
        public void AddOrReplaceRange(IEnumerable<KeyValuePair<TKey, TEntity>> entities, TimeSpan lifeTime)
        {
            if (entities == null)
                throw new ArgumentException(nameof(entities));

            lock (_lock)
            {
                foreach (var kvp in entities)
                {
                    if (Storage.ContainsKey(kvp.Key))
                        Storage[kvp.Key] = new EntityWrapper(kvp.Value, DateTime.Now.Add(lifeTime));
                    else
                        Storage.Add(kvp.Key, new EntityWrapper(kvp.Value, DateTime.Now.Add(lifeTime)));
                }
            }
        }

        /// <summary>
        /// Remove entities entity from the cache.
        /// </summary>
        public void Remove(IEnumerable<TKey> keys)
        {
            if (keys == null)
                throw new ArgumentException(nameof(keys));

            lock (_lock)
            {
                foreach (var key in keys)
                    Storage.Remove(key);
            }
        }

        /// <summary>
        /// Get entity from cache. Return null if key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TEntity Get(TKey key)
        {
            if (key == null)
                return default(TEntity);

            lock (_lock)
            {
                if (!Storage.ContainsKey(key))
                    return default(TEntity);
                return Storage[key].Entity;
            }
        }

        /// <summary>
        /// Get all entities from the cache.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TEntity> GetAll()
        {
            lock (_lock)
            {
                return Storage.Values.Select(x => x.Entity);
            }
        }

        /// <summary>
        /// Get entity from cache. Load entity if key does not exist.
        /// </summary>
        public TEntity GetOrLoad(TKey key)
        {
            if (key == null)
                return default(TEntity);

            if (LoadEntity == null)
                throw new ArgumentException(nameof(LoadEntity));

            lock (_lock)
            {
                if (!Storage.ContainsKey(key))
                    AddOrReplace(key, LoadEntity(key));

                return Storage[key].Entity;
            }
        }

        /// <summary>
        /// Get entities from cache. Load entities whose key does not exist.
        /// </summary>
        public IEnumerable<TEntity> GetOrLoad(params TKey[] keys)
        {
            return GetOrLoad(keys as IEnumerable<TKey>);
        }

        /// <summary>
        /// Get entities from cache. Load entities whose key does not exist.
        /// </summary>
        public IEnumerable<TEntity> GetOrLoad(IEnumerable<TKey> keys)
        {
            if (keys == null)
                return Enumerable.Empty<TEntity>();

            if (LoadEntities == null)
                throw new ArgumentException(nameof(LoadEntities) + " is required.");

            IEnumerable<TKey> notExistedKeys;
            lock (_lock)
            {
                notExistedKeys = keys.Where(x => !Storage.ContainsKey(x));
            }

            var entities = LoadEntities(notExistedKeys);
            lock (_lock)
            {
                foreach (var kvp in entities)
                    AddOrReplace(kvp.Key, kvp.Value);

                return keys.Select(x => Storage[x].Entity);
            }
        }

        public TEntity this[TKey key]
        {
            get
            {
                lock (_lock)
                {
                    return AutoLoadIfNotExisted ? GetOrLoad(key) : Get(key);
                }
            }
            set
            {
                lock (_lock)
                {
                    AddOrReplace(key, value);
                }
            }
        }

        public void Dispose()
        {
            _isDisposed = true;

            // TODO: rewrite this code, it would throw exeption when calling RamCache.Dispose()
            //_garbageCollectorThread.Interrupt();
            //_garbageCollectorThread = null;
            Storage = null;
        }
    }
}
