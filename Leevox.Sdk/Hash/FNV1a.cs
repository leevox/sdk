﻿
namespace Leevox.Sdk.Hash
{
    public class Fnv1A
    {
        private const ulong Fnv64Offset = 0xCBF29CE484222325;
        private const ulong Fnv64Prime = 0x100000001B3;

        public ulong ComputeHash(byte[] bytes)
        {
            ulong hash = Fnv64Offset;

            foreach (var b in bytes)
                hash = Fnv64Prime * (hash ^ b);

            return hash;
        }
    }
}
