﻿using System;
using System.IO;

namespace Leevox.Sdk.Hash
{
    public class Murmur3
    {
        // 128 bit output, 64 bit platform version

        private const int ReadSize = 16;
        private const ulong C1 = 0x87c37b91114253d5L;
        private const ulong C2 = 0x4cf5ad432745937fL;

        public byte[] Murmur3_x64_128(Stream stream, ulong seed = 0)
        {
            ulong remaining;

            if (stream == null || (remaining = (ulong)stream.Length) == 0)
                return null;

            ulong h1 = seed, h2 = seed, length = 0;

            var hash = new byte[ReadSize];

            #region body

            var data = new byte[ReadSize];

            // read 128 bits, 16 bytes, 2 longs in eacy cycle
            while (remaining >= ReadSize)
            {
                stream.Read(data, 0, ReadSize);

                ulong k1 = GetULong(data, 0);

                ulong k2 = GetULong(data, 8);

                length += ReadSize;
                remaining -= ReadSize;

                h1 ^= MixKey1(k1);

                h1 = RotateLeft(h1, 27);
                h1 += h2;
                h1 = h1 * 5 + 0x52dce729;

                h2 ^= MixKey2(k2);

                h2 = RotateLeft(h2, 31);
                h2 += h1;
                h2 = h2 * 5 + 0x38495ab5;
            }

            #endregion

            #region tail

            if (remaining > 0)
            {
                stream.Read(data, 0, ReadSize);

                ulong k1 = 0;
                ulong k2 = 0;
                length += remaining;

                // little endian (x86) processing
                switch (remaining)
                {
                    case 15:
                        k2 ^= (ulong)data[14] << 48; // fall through
                        goto case 14;
                    case 14:
                        k2 ^= (ulong)data[13] << 40; // fall through
                        goto case 13;
                    case 13:
                        k2 ^= (ulong)data[12] << 32; // fall through
                        goto case 12;
                    case 12:
                        k2 ^= (ulong)data[11] << 24; // fall through
                        goto case 11;
                    case 11:
                        k2 ^= (ulong)data[10] << 16; // fall through
                        goto case 10;
                    case 10:
                        k2 ^= (ulong)data[9] << 8; // fall through
                        goto case 9;
                    case 9:
                        k2 ^= (ulong)data[8]; // fall through
                        goto case 8;
                    case 8:
                        k1 ^= GetULong(data, 0);
                        break;
                    case 7:
                        k1 ^= (ulong)data[6] << 48; // fall through
                        goto case 6;
                    case 6:
                        k1 ^= (ulong)data[5] << 40; // fall through
                        goto case 5;
                    case 5:
                        k1 ^= (ulong)data[4] << 32; // fall through
                        goto case 4;
                    case 4:
                        k1 ^= (ulong)data[3] << 24; // fall through
                        goto case 3;
                    case 3:
                        k1 ^= (ulong)data[2] << 16; // fall through
                        goto case 2;
                    case 2:
                        k1 ^= (ulong)data[1] << 8; // fall through
                        goto case 1;
                    case 1:
                        k1 ^= (ulong)data[0]; // fall through
                        break;
                    default:
                        throw new Exception("Something went wrong with remaining bytes calculation.");
                }

                h1 ^= MixKey1(k1);
                h2 ^= MixKey2(k2);
            }

            #endregion

            #region finalization

            h1 ^= length;
            h2 ^= length;

            h1 += h2;
            h2 += h1;

            h1 = MixFinal(h1);
            h2 = MixFinal(h2);

            h1 += h2;
            h2 += h1;

            Array.Copy(BitConverter.GetBytes(h1), 0, hash, 0, 8);
            Array.Copy(BitConverter.GetBytes(h2), 0, hash, 8, 8);

            #endregion

            return hash;
        }

        public byte[] Murmur3_x64_128(byte[] data, ulong seed = 0)
        {
            if (data == null || data.Length == 0)
                return null;

            ulong h1 = seed, h2 = seed, length = 0;

            var hash = new byte[ReadSize];

            #region body

            int pos = 0;
            var remaining = (ulong)data.Length;

            // read 128 bits, 16 bytes, 2 longs in eacy cycle
            while (remaining >= ReadSize)
            {
                ulong k1 = GetULong(data, pos);
                pos += 8;

                ulong k2 = GetULong(data, pos);
                pos += 8;

                length += ReadSize;
                remaining -= ReadSize;

                h1 ^= MixKey1(k1);

                h1 = RotateLeft(h1, 27);
                h1 += h2;
                h1 = h1 * 5 + 0x52dce729;

                h2 ^= MixKey2(k2);

                h2 = RotateLeft(h2, 31);
                h2 += h1;
                h2 = h2 * 5 + 0x38495ab5;
            }

            #endregion

            #region tail

            if (remaining > 0)
            {
                ulong k1 = 0;
                ulong k2 = 0;
                length += remaining;

                // little endian (x86) processing
                switch (remaining)
                {
                    case 15:
                        k2 ^= (ulong)data[pos + 14] << 48; // fall through
                        goto case 14;
                    case 14:
                        k2 ^= (ulong)data[pos + 13] << 40; // fall through
                        goto case 13;
                    case 13:
                        k2 ^= (ulong)data[pos + 12] << 32; // fall through
                        goto case 12;
                    case 12:
                        k2 ^= (ulong)data[pos + 11] << 24; // fall through
                        goto case 11;
                    case 11:
                        k2 ^= (ulong)data[pos + 10] << 16; // fall through
                        goto case 10;
                    case 10:
                        k2 ^= (ulong)data[pos + 9] << 8; // fall through
                        goto case 9;
                    case 9:
                        k2 ^= (ulong)data[pos + 8]; // fall through
                        goto case 8;
                    case 8:
                        k1 ^= GetULong(data, pos);
                        break;
                    case 7:
                        k1 ^= (ulong)data[pos + 6] << 48; // fall through
                        goto case 6;
                    case 6:
                        k1 ^= (ulong)data[pos + 5] << 40; // fall through
                        goto case 5;
                    case 5:
                        k1 ^= (ulong)data[pos + 4] << 32; // fall through
                        goto case 4;
                    case 4:
                        k1 ^= (ulong)data[pos + 3] << 24; // fall through
                        goto case 3;
                    case 3:
                        k1 ^= (ulong)data[pos + 2] << 16; // fall through
                        goto case 2;
                    case 2:
                        k1 ^= (ulong)data[pos + 1] << 8; // fall through
                        goto case 1;
                    case 1:
                        k1 ^= (ulong)data[pos]; // fall through
                        break;
                    default:
                        throw new Exception("Something went wrong with remaining bytes calculation.");
                }

                h1 ^= MixKey1(k1);
                h2 ^= MixKey2(k2);
            }

            #endregion

            #region finalization

            h1 ^= length;
            h2 ^= length;

            h1 += h2;
            h2 += h1;

            h1 = MixFinal(h1);
            h2 = MixFinal(h2);

            h1 += h2;
            h2 += h1;

            Array.Copy(BitConverter.GetBytes(h1), 0, hash, 0, 8);
            Array.Copy(BitConverter.GetBytes(h2), 0, hash, 8, 8);

            #endregion

            return hash;
        }

        private ulong MixKey1(ulong k1)
        {
            k1 *= C1;
            k1 = RotateLeft(k1, 31);
            k1 *= C2;
            return k1;
        }

        private ulong MixKey2(ulong k2)
        {
            k2 *= C2;
            k2 = RotateLeft(k2, 33);
            k2 *= C1;
            return k2;
        }

        private ulong MixFinal(ulong k)
        {
            k ^= k >> 33;
            k *= 0xff51afd7ed558ccdL;
            k ^= k >> 33;
            k *= 0xc4ceb9fe1a85ec53L;
            k ^= k >> 33;
            return k;
        }

        private ulong RotateLeft(ulong original, int bits)
        {
            return (original << bits) | (original >> (64 - bits));
        }

        unsafe private ulong GetULong(byte[] bb, int pos)
        {
            // we only read aligned longs, so a simple casting is enough
            fixed (byte* pbyte = &bb[pos])
            {
                return *((ulong*)pbyte);
            }
        }
    }
}
