﻿using System.Data.SqlClient;

namespace Leevox.Sdk.Sql
{
    public class SqlDb : ISqlDb
    {
        internal SqlConnection SqlConnection { get; set; }

        public string ConnectionString
        {
            get { return SqlConnection?.ConnectionString ?? string.Empty; }
        }

        public SqlDb(string connectionString)
        {
            SqlConnection = new SqlConnection(connectionString);
            SqlConnection.Open();
        }

        public void Dispose()
        {
            SqlConnection.Dispose();
        }

        /// <summary>
        /// Represents a Transact-SQL statement to execute against a SQL Server database.
        /// </summary>
        /// <param name="sql">SQL statement</param>
        public ISqlCmd Sql(string sql)
        {
            return new SqlCmd(this, sql, SqlCmdType.Text);
        }

        /// <summary>
        /// Represents a Transact-SQL stored procedure to execute against a SQL Server database.
        /// </summary>
        /// <param name="storedProcedure">Stored procedure name</param>
        public ISqlCmd Procedure(string storedProcedure)
        {
            return new SqlCmd(this, storedProcedure, SqlCmdType.StoredProcedure);
        }
    }
}
