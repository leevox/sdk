﻿using System;

namespace Leevox.Sdk.Sql
{
    public interface ISqlDb : IDisposable
    {
        /// <summary>
        /// Represents a Transact-SQL statement to execute against a SQL Server database.
        /// </summary>
        /// <param name="sql">SQL statement</param>
        ISqlCmd Sql(string sql);

        /// <summary>
        /// Represents a Transact-SQL stored procedure to execute against a SQL Server database.
        /// </summary>
        /// <param name="storedProcedure">Stored procedure name</param>
        ISqlCmd Procedure(string storedProcedure);
    }
}