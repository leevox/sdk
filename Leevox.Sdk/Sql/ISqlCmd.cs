﻿using System;
using System.Collections.Generic;

namespace Leevox.Sdk.Sql
{
    public interface ISqlCmd : IDisposable
    {
        string CommandText { get; }

        /// <summary>
        /// Add a parameter with value
        /// </summary>
        ISqlCmd Set(string parameterName, object value);
        /// <summary>
        /// Dynamic add parameter(s)
        /// </summary>
        ISqlCmd Set(object parameters);

        /// <summary>
        /// Execute readr the command text or stored procedure.
        /// </summary>
        IEnumerable<ISqlRow> Execute();
        /// <summary>
        /// Add parameter(s) to the command text or stored procedure then execute reader.
        /// </summary>
        IEnumerable<ISqlRow> Execute(object parameters);

        /// <summary>
        /// Execute non query the command text or stored procedure.
        /// </summary>
        int ExecuteNonQuery();
        /// <summary>
        /// Add parameter(s) to the command text or stored procedure then execute non query.
        /// </summary>
        int ExecuteNonQuery(object parameters);
        /// <summary>
        /// Execute non query multiple times with parameter(s)
        /// </summary>
        int ExecuteNonQuery(params object[] parameters);

        /// <summary>
        /// Execute scalar the command text or stored procedure.
        /// </summary>
        T ExecuteScalar<T>();
        /// <summary>
        /// Add parameter(s) to the command text or stored procedure then execute scalar.
        /// </summary>
        T ExecuteScalar<T>(object parameters);
    }
}