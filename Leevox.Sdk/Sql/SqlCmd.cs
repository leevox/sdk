﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Leevox.Sdk.Util;

namespace Leevox.Sdk.Sql
{
    internal enum SqlCmdType
    {
        Text,
        StoredProcedure
    }

    public class SqlCmd : ISqlCmd
    {
        internal SqlCommand SqlCommand { get; set; }

        internal SqlCmdType CommandType { get; set; }

        internal ISqlDb Database { get; set; }

        public string CommandText { get; private set; }

        internal SqlCmd(SqlDb database, string command, SqlCmdType type)
        {
            Database = database;
            CommandText = command;
            CommandType = type;

            SqlCommand = new SqlCommand(command, database.SqlConnection)
            {
                CommandType =
                    type == SqlCmdType.StoredProcedure
                        ? System.Data.CommandType.StoredProcedure
                        : System.Data.CommandType.Text
            };
        }

        public ISqlCmd Set(string parameterName, object value)
        {
            var values = value as IEnumerable;
            if (values != null)
            {
                var trimName = parameterName.TrimOrEmpty(' ', '@');
                var replace = string.Empty;
                var index = 1;

                foreach (var item in values)
                {
                    if (index > 2000)
                        throw new ArgumentException("Maximum array length is 2000");

                    var param = '@' + trimName + index++;
                    replace += param + ',';
                    SqlCommand.Parameters.AddWithValue(param, item);
                }
                SqlCommand.CommandText = Regex.Replace(
                    SqlCommand.CommandText,
                    "@*" + trimName,
                    replace.TrimEnd(','),
                    RegexOptions.IgnoreCase
                );
            }
            else
            {
                SqlCommand.Parameters.AddWithValue(parameterName.TrimOrEmpty(' ', '@'), value);
            }
            
            return this;
        }

        public ISqlCmd Set(object parameters)
        {
            foreach (var property in parameters.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                Set(property.Name, property.GetValue(parameters, null));
            }
            
            return this;
        }

        public IEnumerable<ISqlRow> Execute()
        {
            using (var reader = SqlCommand.ExecuteReader())
            {
                while (reader.Read())
                    yield return new SqlRow(reader);
            }
        }

        public IEnumerable<ISqlRow> Execute(object parameters)
        {
            return Set(parameters).Execute();
        }

        public int ExecuteNonQuery()
        {
            return SqlCommand.ExecuteNonQuery();
        }

        public int ExecuteNonQuery(object parameters)
        {
            return Set(parameters).ExecuteNonQuery();
        }

        public int ExecuteNonQuery(params object[] parameters)
        {
            return parameters.Sum(p => Set(p).ExecuteNonQuery());
        }

        public T ExecuteScalar<T>()
        {
            return (T)SqlCommand.ExecuteScalar();
        }

        public T ExecuteScalar<T>(object parameters)
        {
            return Set(parameters).ExecuteScalar<T>();
        }

        public void Dispose()
        {
            SqlCommand.Dispose();
        }
    }
}
