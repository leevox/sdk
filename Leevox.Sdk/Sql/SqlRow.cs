﻿using System;
using System.Data.SqlClient;
using Leevox.Sdk.Util;

namespace Leevox.Sdk.Sql
{
    public class SqlRow : ISqlRow
    {
        private SqlDataReader Reader { get; set; }

        public SqlRow(SqlDataReader reader)
        {
            Reader = reader;
        }

        public T Get<T>(string column)
        {
            column = column.TrimOrEmpty(' ', '[', ']');
            return (T) Reader.GetValue(Reader.GetOrdinal(column));
        }

        public T Get<T>(string column, T defaultValue)
        {
            try
            {
                return Get<T>(column);
            }
            catch
            {
                return defaultValue;
            }
        }

        public string GetString(string column)
        {
            return Get<string>(column);
        }

        public string GetString(string column, string defaultValue)
        {
            return Get(column, defaultValue);
        }

        public bool GetBoolean(string column)
        {
            return Get<bool>(column);
        }

        public bool GetBoolean(string column, bool defaultValue)
        {
            return Get(column, defaultValue);
        }

        public DateTime GetDateTime(string column)
        {
            return Get<DateTime>(column);
        }

        public DateTime GetDateTime(string column, DateTime defaultValue)
        {
            return Get(column, defaultValue);
        }

        public byte GetByte(string column)
        {
            return Get<byte>(column);
        }

        public byte GetByte(string column, byte defaultValue)
        {
            return Get(column, defaultValue);
        }

        public int GetInt(string column)
        {
            return Get<int>(column);
        }

        public int GetInt(string column, int defaultValue)
        {
            return Get(column, defaultValue);
        }

        public long GetLong(string column)
        {
            return Get<long>(column);
        }

        public long GetLong(string column, long defaultValue)
        {
            return Get(column, defaultValue);
        }

        public float GetFloat(string column)
        {
            return Get<float>(column);
        }

        public float GetFloat(string column, float defaultValue)
        {
            return Get(column, defaultValue);
        }

        public double GetDouble(string column)
        {
            return Get<double>(column);
        }

        public double GetDouble(string column, double defaultValue)
        {
            return Get(column, defaultValue);
        }

        public decimal GetDecimal(string column)
        {
            return Get<decimal>(column);
        }

        public decimal GetDecimal(string column, decimal defaultValue)
        {
            return Get(column, defaultValue);
        }
    }
}