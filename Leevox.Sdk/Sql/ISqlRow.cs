﻿using System;

namespace Leevox.Sdk.Sql
{
    public interface ISqlRow
    {
        /// <summary>
        /// Get the value of the specific column
        /// </summary>
        /// <param name="column">Column name</param>
        T Get<T>(string column);
        /// <summary>
        /// Get the value of the specific column, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        T Get<T>(string column, T defaultValue);

        /// <summary>
        /// Get the value of the specific column as string
        /// </summary>
        /// <param name="column">Column name</param>
        string GetString(string column);
        /// <summary>
        /// Get the value of the specific column as string, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        string GetString(string column, string defaultValue);

        /// <summary>
        /// Get the value of the specific column as bool
        /// </summary>
        /// <param name="column">Column name</param>
        bool GetBoolean(string column);
        /// <summary>
        /// Get the value of the specific column as bool, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        bool GetBoolean(string column, bool defaultValue);

        /// <summary>
        /// Get the value of the specific column as DateTime
        /// </summary>
        /// <param name="column">Column name</param>
        DateTime GetDateTime(string column);
        /// <summary>
        /// Get the value of the specific column as DateTime, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        DateTime GetDateTime(string column, DateTime defaultValue);

        /// <summary>
        /// Get the value of the specific column as byte
        /// </summary>
        /// <param name="column">Column name</param>
        byte GetByte(string column);
        /// <summary>
        /// Get the value of the specific column as byte, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        byte GetByte(string column, byte defaultValue);

        /// <summary>
        /// Get the value of the specific column as int
        /// </summary>
        /// <param name="column">Column name</param>
        int GetInt(string column);
        /// <summary>
        /// Get the value of the specific column as int, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        int GetInt(string column, int defaultValue);

        /// <summary>
        /// Get the value of the specific column as long
        /// </summary>
        /// <param name="column">Column name</param>
        long GetLong(string column);
        /// <summary>
        /// Get the value of the specific column as long, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        long GetLong(string column, long defaultValue);

        /// <summary>
        /// Get the value of the specific column as float
        /// </summary>
        /// <param name="column">Column name</param>
        float GetFloat(string column);
        /// <summary>
        /// Get the value of the specific column as float, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        float GetFloat(string column, float defaultValue);

        /// <summary>
        /// Get the value of the specific column as double
        /// </summary>
        /// <param name="column">Column name</param>
        double GetDouble(string column);
        /// <summary>
        /// Get the value of the specific column as double, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        double GetDouble(string column, double defaultValue);

        /// <summary>
        /// Get the value of the specific column as decimal
        /// </summary>
        /// <param name="column">Column name</param>
        decimal GetDecimal(string column);
        /// <summary>
        /// Get the value of the specific column as decimal, return default value if any exception come.
        /// </summary>
        /// <param name="column">Column name</param>
        /// <param name="defaultValue">Return value if any exception come</param>
        decimal GetDecimal(string column, decimal defaultValue);
    }
}