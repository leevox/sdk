﻿using System.Diagnostics;
using Leevox.Sdk.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Leevox.Sdk.Test.Util
{
    [TestClass]
    public class RandomNumberUtilTest
    {
        [TestMethod]
        public void TestRandomDouble()
        {
            for (int i = 0; i < 100; i++)
            {
                var r = RandomNumber.RandomInt(int.MinValue,int.MaxValue);
                Debug.Print(r.ToString());
            }
        }
    }
}
