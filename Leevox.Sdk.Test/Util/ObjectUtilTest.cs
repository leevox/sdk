﻿using Leevox.Sdk.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Leevox.Sdk.Test.Util
{
    [TestClass]
    public class ObjectUtilTest
    {
        [TestMethod]
        public void TestFnv1A()
        {
            string data = "123456";
            var expectedResult = 0xF6E3ED7E0E67290A;
            var result = data.Fnv1A();
            Assert.AreEqual(expectedResult, result);

            data = "abcdef";
            expectedResult = 0xD80BDA3FBE244A0A;
            result = data.Fnv1A();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestFnv1AFile()
        {
            var file = @"Resource\Hello.txt";
            var expectedResult = 0x324B4CC6A307FD19UL;
            var result = (File.OpenRead(file)).Fnv1A();
            Assert.AreEqual(expectedResult, result);

            file = @"Resource\Desert.jpg";
            expectedResult = 0xCF66051BF8745799UL;
            result = (File.OpenRead(file)).Fnv1A();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestMurmur3()
        {
            string data = "123456";
            var expectedResult = "D6D0BB0B05CF17E4FE3125009180A451";
            var result = data.Murmur3().ToHexString();
            Assert.AreEqual(expectedResult, result);

            data = "abcdef";
            expectedResult = "55BFA3ACBF867DE45C842133990971B0";
            result = data.Murmur3().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestMurmur3File()
        {
            var file = @"Resource\Hello.txt";
            var expectedResult = "251C7C748A07F70BF639B29B5EFAB16A";
            var result = (File.OpenRead(file)).Murmur3().ToHexString();
            Assert.AreEqual(expectedResult, result);

            file = @"Resource\Desert.jpg";
            expectedResult = "481B0EC0A935057AB41AACF8CB84EEE2";
            result = (File.OpenRead(file)).Murmur3().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestMd5()
        {
            var data = "123456";
            var expectedResult = "E10ADC3949BA59ABBE56E057F20F883E";
            var result = data.Md5().ToHexString();
            Assert.AreEqual(expectedResult, result);

            data = "abcdef";
            expectedResult = "E80B5017098950FC58AAD83C8C14978E";
            result = data.Md5().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestMd5File()
        {
            var file = @"Resource\Hello.txt";
            var expectedResult = "7DD19DFEFAE5A697EA9AC4A05A47D152";
            var result = (File.OpenRead(file)).Md5().ToHexString();
            Assert.AreEqual(expectedResult, result);

            file = @"Resource\Desert.jpg";
            expectedResult = "BA45C8F60456A672E003A875E469D0EB";
            result = (File.OpenRead(file)).Md5().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestSha1()
        {
            var data = "123456";
            var expectedResult = "7C4A8D09CA3762AF61E59520943DC26494F8941B";
            var result = data.Sha1().ToHexString();
            Assert.AreEqual(expectedResult, result);

            data = "abcdef";
            expectedResult = "1F8AC10F23C5B5BC1167BDA84B833E5C057A77D2";
            result = data.Sha1().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestSha1File()
        {
            var file = @"Resource\Hello.txt";
            var expectedResult = "2ED28475F227630946CD8EF13FAD5D0A4F85EFF7";
            var result = (File.OpenRead(file)).Sha1().ToHexString();
            Assert.AreEqual(expectedResult, result);

            file = @"Resource\Desert.jpg";
            expectedResult = "30420D1A9AFB2BCB60335812569AF4435A59CE17";
            result = (File.OpenRead(file)).Sha1().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestSha256()
        {
            var data = "123456";
            var expectedResult = "8D969EEF6ECAD3C29A3A629280E686CF0C3F5D5A86AFF3CA12020C923ADC6C92";
            var result = data.Sha256().ToHexString();
            Assert.AreEqual(expectedResult, result);

            data = "abcdef";
            expectedResult = "BEF57EC7F53A6D40BEB640A780A639C83BC29AC8A9816F1FC6C5C6DCD93C4721";
            result = data.Sha256().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestSha256File()
        {
            var file = @"Resource\Hello.txt";
            var expectedResult = "5FCC70FB6DB347EE1041E37DF778A7F43BD74366C8885BE7E6C8F0501F2A4DC8";
            var result = (File.OpenRead(file)).Sha256().ToHexString();
            Assert.AreEqual(expectedResult, result);

            file = @"Resource\Desert.jpg";
            expectedResult = "010F60D2927A35D0235490136EF9F4953B7EE453073794BCAF153D20A64544EA";
            result = (File.OpenRead(file)).Sha256().ToHexString();
            Assert.AreEqual(expectedResult, result);
        }
    }
}
