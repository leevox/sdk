﻿using Leevox.Sdk.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Leevox.Sdk.Test.Util
{
    [TestClass]
    public class StringUtilTest
    {
        [TestMethod]
        public void TestIsEqual()
        {
            const string s1 = "Đây là Unicode Việt Nam";
            const string s2 = "ĐÂY là Unicode VIỆT NAM";
            const string s3 = "Đây là Unicode Việt Nam";

            var result = s1.IsOrdinalEqual(s2);

            Assert.AreEqual(result, false);

            result = s1.IsOrdinalEqual(s3);
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void TestIsEqualIgnorecase()
        {
            const string s1 = "Đây là Unicode Việt Nam";
            const string s2 = "ĐÂY là Unicode VIỆT NAM";
            const string s3 = "Đây là Unicode Việt Nam";

            var result = s1.IsOrdinalEqualIgnoreCase(s2);

            Assert.AreEqual(result, true);

            result = s1.IsOrdinalEqualIgnoreCase(s3);
            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void TestTrimOrEmpty()
        {
            const string notNullString = "    \t   \n    hello world    \r      ";

            const string expectedResult = "hello world";
            string result = notNullString.TrimOrEmpty();

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestTrimOrEmptyParams()
        {
            const string notNullString = "    \t   \n hello world\t\r      ";

            const string expectedResult = "\n hello world\t\r";
            string result = notNullString.TrimOrEmpty(' ', '\t');

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestTrimOrEmptyNull()
        {
            string nullString = null;

            string expectedResult = string.Empty;
            string result = nullString.TrimOrEmpty();

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestIsNullOrWhiteSpace()
        {
            string str = null;
            Assert.AreEqual(string.IsNullOrWhiteSpace(str), str.IsNullOrWhiteSpace());

            str = "A";
            Assert.AreEqual(string.IsNullOrWhiteSpace(str), str.IsNullOrWhiteSpace());
        }

        [TestMethod]
        public void TestArgs()
        {
            DateTime now = DateTime.Now;
            const string pattern = "Now is {0} h {1} m {2} s";

            string expectedResult = string.Format(pattern, now.Hour, now.Minute, now.Second);
            string result = pattern.Format(now.Hour, now.Minute, now.Second);

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void TestArgsNull()
        {
            DateTime now = DateTime.Now;
            string pattern = null;

            string expectedResult = string.Empty;
            string result = pattern.Format(now.Hour, now.Minute, now.Second);

            Assert.AreEqual(expectedResult, result);
        }
    }
}
