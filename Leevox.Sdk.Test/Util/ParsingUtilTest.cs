﻿using Leevox.Sdk.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Leevox.Sdk.Test.Util
{
    [TestClass]
    public class ParsingUtilTest
    {
        [TestMethod]
        public void TestIntParse()
        {
            var input = "dsad";
            var result = input.ToInt();

            Assert.AreEqual(result, null);
        }
    }
}
