﻿using Leevox.Sdk.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Leevox.Sdk.Test.Util
{
    [TestClass]
    public class StringComparerUtilTest
    {
        [TestMethod]
        public void TestOrdinalIgnoreSpaces()
        {
            const string s1 = "   abc";
            const string s2 = "abc";
            const string s3 = "abc   ";
            const string s4 = "a b c";

            var set = new HashSet<string>(StringComparerUtil.OrdinalIgnoreSpaces);
            set.Add(s1);

            Assert.IsTrue(set.Contains(s2), "s1 == s2");
            Assert.IsTrue(set.Contains(s3), "s1 == s3");
            Assert.IsFalse(set.Contains(s4), "s1 != s4");
        }

        [TestMethod]
        public void TestOrdinalIgnoreCaseAndSpaces()
        {
            const string s1 = "   abc";
            const string s2 = "aBc";
            const string s3 = "aBC   ";
            const string s4 = "A b C";

            var set = new HashSet<string>(StringComparerUtil.OrdinalIgnoreCaseAndSpaces);
            set.Add(s1);

            Assert.IsTrue(set.Contains(s2), "s1 == s2");
            Assert.IsTrue(set.Contains(s3), "s1 == s3");
            Assert.IsFalse(set.Contains(s4), "s1 != s4");
        }
    }
}
