﻿using System;
using System.Diagnostics;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Leevox.Sdk.Test.Resource
{
    [TestClass]
    public class TestResource
    {
        [TestMethod]
        public void TestMethod1()
        {
            var r = new Leevox.Resource.Resource();
            Debug.Print(r.GetText("def", "vi-vn"));

            r.AddText("xyz1", "vi-vn", "vn xyz " + DateTime.Now.ToString(CultureInfo.InvariantCulture));
            Debug.Print(r["xyz", "fr-fr"]);

            r.Dispose();
        }
    }
}
