﻿using Leevox.Sdk.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading;

namespace Leevox.Sdk.Test.Cache
{
    [TestClass]
    public class TestCache
    {
        [TestMethod]
        public void TestCache1()
        {
            var cache = new RamCache<int, string>(
                x => x.ToString(),
                x => x.ToDictionary(x1 => x1, x1 => x1.ToString())
            )
            {
                DefaultLifeTime = TimeSpan.FromSeconds(5),
                RefreshInterval = TimeSpan.FromSeconds(2)
            };

            cache.Add(1, "1");
            cache.Add(2, "2");
            
            cache.AddOrReplace(2, "3");

            cache.GetOrLoad(1, 2, 3 );

            Assert.AreEqual("3", cache.Get(2));

            Thread.Sleep(7000);

            Assert.AreEqual("2", cache.GetOrLoad(2));
        }
    }
}
