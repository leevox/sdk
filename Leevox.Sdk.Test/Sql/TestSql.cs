﻿using Leevox.Sdk.Sql;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Leevox.Sdk.Test.Sql
{
    [TestClass]
    public class TestSql
    {
        private const string ConnectionString = @"Server=.\SqlExpress;Database=AdventureWorks;Trusted_Connection=Yes;";

        [TestMethod]
        public void TestExecuteRange()
        {
            var allCustomers = new List<object>();

            using (var db = new SqlDb(ConnectionString))
            {
                allCustomers.AddRange(
                    db.Sql("select * from sales.customer where customerid in (@iDs)")
                        .Execute(new { Ids = Enumerable.Range(1, 2000)})
                        .Select(row => new
                        {
                            CustomerId = row.GetInt("CustomerId"),
                            AccountNumber = row.GetString("[AccountNumber]"),
                            ModifiedDate = row.GetDateTime("ModifiedDate")
                        })
                    );
            }

            Assert.IsTrue(allCustomers.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException), "Maximum array length is 2000")]
        public void TestExecuteOutOfRange()
        {
            using (var db = new SqlDb(ConnectionString))
            {
                db.Sql("select * from sales.customer where customerid in (@iDs)")
                    .Execute(new {Ids = Enumerable.Range(1, 2001)});
            }
        }
    }
}
