﻿using System.Globalization;

namespace Leevox.Resource
{
    public interface IResource
    {
        /// <summary>
        /// Get resource using CurrentCulture.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string GetText(string key);

        /// <summary>
        /// Get resource using specific culture name.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <returns></returns>
        string GetText(string key, string culture);

        /// <summary>
        /// Get resource using specific culture.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        string GetText(string key, CultureInfo culture);

        /// <summary>
        /// Add new resource using CurrentCulture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        void AddText(string key, string value);

        /// <summary>
        /// Add new resource using specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <param name="value"></param>
        /// <returns></returns>
        void AddText(string key, string culture, string value);

        /// <summary>
        /// Add new resource using specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        void AddText(string key, CultureInfo culture, string value);

        /// <summary>
        /// Remove resource, all culture.
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);

        /// <summary>
        /// Remove resource, specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        void Remove(string key, string culture);

        /// <summary>
        /// Remove resource, specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        void Remove(string key, CultureInfo culture);

        /// <summary>
        /// Get or set resource using CurrentCulture.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        string this[string key] { get; set; }

        /// <summary>
        /// Get or set resource using specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <returns></returns>
        string this[string key, string culture] { get; set; }

        /// <summary>
        /// Get or set resource using specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        string this[string key, CultureInfo culture] { get; set; }
    }
}
