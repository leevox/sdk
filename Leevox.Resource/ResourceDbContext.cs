﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.SqlServer;

namespace Leevox.Resource
{
    interface IResourceDbContext
    {
        DbSet<ResourceDto> Resource { get; set; }
    }

    public class SqlServerConfiguration : DbConfiguration
    {
        public SqlServerConfiguration()
        {
            // config Entity Framework programaticly, not using app.config file.
            SetProviderServices(SqlProviderServices.ProviderInvariantName, SqlProviderServices.Instance);
        }
    }

    public class ResourceDto
    {
        [Column("Key", Order = 0), Key]
        public string Key { get; set; }
        
        [Column("Culture", Order = 1), Key, Required]
        public string Culture { get; set; }
        
        [Column("Value", Order = 2), Required]
        public string Value { get; set; }
    }

    public class ResourceProvider : DbContext, IResourceDbContext
    {
        public string TableName { get; private set; }

        public ResourceProvider(string tableName)
        {
            TableName = tableName;
        }

        public ResourceProvider(string tableName, string connectionStringName)
            : base(connectionStringName)
        {
            TableName = tableName;
        }

        public ResourceProvider(string tableName, DbConnection connectionString)
            : base(connectionString, true)
        {
            TableName = tableName;
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ResourceDto>().ToTable(TableName);
        }

        public DbSet<ResourceDto> Resource { get; set; }
    }
}
