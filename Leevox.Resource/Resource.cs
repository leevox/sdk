﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Leevox.Sdk.Cache;
using Leevox.Sdk.Util;

namespace Leevox.Resource
{
    public class Resource : IResource, IDisposable
    {
        public static readonly CultureInfo Vietnam = CultureInfo.GetCultureInfo("vi-vn");
        public static readonly CultureInfo Usa = CultureInfo.GetCultureInfo("en-us");

        private ResourceProvider DbContext { get; set; }

        private readonly Dictionary<string, CultureInfo> CultureLookup = CultureInfo.GetCultures(CultureTypes.AllCultures)
            .ToDictionary(x => x.Name, x => x, StringComparer.OrdinalIgnoreCase);

        private static RamCache<string, Dictionary<CultureInfo, string>> Storage { get; set; }

        public Resource(string tableName = "Resource", string connectionStringName = "ResourceProvider")
        {
            DbContext = new ResourceProvider(tableName, connectionStringName);

            Storage = new RamCache<string, Dictionary<CultureInfo, string>>(
                LoadResource,
                LoadResources,
                StringComparer.OrdinalIgnoreCase
            )
            {
                AutoLoadIfNotExisted = true,
                AutoReloadIfExpired = true,
                DefaultLifeTime = TimeSpan.FromMinutes(20),
                RefreshInterval = TimeSpan.FromMinutes(5)
            };

            // load all resources
            Storage.AddOrReplaceRange(CombineRows(DbContext.Resource));
        }

        private IEnumerable<KeyValuePair<string, Dictionary<CultureInfo, string>>> CombineRows(IEnumerable<ResourceDto> resourceDto)
        {
            return resourceDto
                    .GroupBy(x => x.Key).ToDictionary(
                        g => g.Key, // key
                        g => g.GroupBy(x => x.Culture).ToDictionary(
                            x => CultureInfo.GetCultureInfo(x.Key), // culture
                            x => x.ToList().FirstOrDefault()?.Value // value
                        )
                    );
        }

        private IEnumerable<KeyValuePair<string, Dictionary<CultureInfo, string>>> LoadResources(IEnumerable<string> keys)
        {
            var rows = DbContext.Resource.Where(x => keys.Contains(x.Key, StringComparer.OrdinalIgnoreCase));
            return CombineRows(rows);
        }

        private Dictionary<CultureInfo, string> LoadResource(string key)
        {
            var rows = DbContext.Resource.Where(x => string.Compare(x.Key, key, StringComparison.OrdinalIgnoreCase) == 0);
            return CombineRows(rows).FirstOrDefault().Value;
        }

        /// <summary>
        /// Get resource using CurrentCulture.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetText(string key)
        {
            return GetText(key, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Get resource using specific culture name.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <returns></returns>
        public string GetText(string key, string culture)
        {
            if (CultureLookup.ContainsKey(culture))
                return GetText(key, CultureLookup[culture]);
            return key;
        }

        /// <summary>
        /// Get resource using specific culture.
        /// Return string.Empty if resource key does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public string GetText(string key, CultureInfo culture)
        {
            var ret = Storage.GetOrLoad(key);
            if (ret == null || !ret.ContainsKey(culture))
                return key;
            return ret[culture];
        }

        /// <summary>
        /// Add new resource using CurrentCulture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void AddText(string key, string value)
        {
            AddText(key, CultureInfo.CurrentCulture, value);
        }

        /// <summary>
        /// Add new resource using specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void AddText(string key, string culture, string value)
        {
            if (!CultureLookup.ContainsKey(culture))
                throw new Exception("Culture name is not correct.");
            AddText(key, CultureLookup[culture], value);
        }

        /// <summary>
        /// Add new resource using specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public void AddText(string key, CultureInfo culture, string value)
        {
            var dto = new ResourceDto {
                Key = key,
                Culture = culture.Name,
                Value = value
            };
            var existingDto = DbContext.Resource.FirstOrDefault(x =>
                string.Compare(x.Key, dto.Key, StringComparison.OrdinalIgnoreCase) == 0
                && string.Compare(x.Culture, dto.Culture, StringComparison.OrdinalIgnoreCase) == 0
            );
            if (existingDto != null)
            {
                existingDto.Value = dto.Value;
            }
            else
            {
                DbContext.Resource.Add(dto);
            }
            DbContext.SaveChanges();
            RefreshCache(key, culture, value);
        }

        /// <summary>
        /// Remove resource, all culture.
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            var entities = DbContext.Resource.Where(x => x.Key == key);
            if (entities.Any())
            {
                DbContext.Resource.RemoveRange(entities);
                DbContext.SaveChanges();
            }
            Storage.Remove(key);
        }

        /// <summary>
        /// Remove resource, specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        public void Remove(string key, string culture)
        {
            var entity = DbContext.Resource.FirstOrDefault(x => x.Key == key && x.Culture == culture);
            if (entity != null)
            {
                DbContext.Resource.Remove(entity);
                DbContext.SaveChanges();
            }
            Storage.Remove(key);
        }

        /// <summary>
        /// Remove resource, specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        public void Remove(string key, CultureInfo culture)
        {
            Remove(key, culture.Name);
        }

        /// <summary>
        /// Set default culture to the whole application
        /// </summary>
        /// <param name="cultureName"></param>
        /// <returns></returns>
        public static bool SetDefaultCulture(string cultureName)
        {
            var culture = CultureInfo.GetCultureInfo(cultureName);
            if (culture.Name.IsNullOrWhiteSpace())
            {
                return false;
            }
            else
            {
                CultureInfo.DefaultThreadCurrentCulture = culture;
                CultureInfo.DefaultThreadCurrentUICulture = culture;
                return true;
            }
        }

        /// <summary>
        /// Get or set resource using CurrentCulture.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[string key]
        {
            get { return GetText(key); }
            set { AddText(key, value); }
        }

        /// <summary>
        /// Get or set resource using specific culture name.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture">name of the culture, ignore case</param>
        /// <returns></returns>
        public string this[string key, string culture]
        {
            get { return GetText(key, culture); }
            set { AddText(key, culture, value); }
        }

        /// <summary>
        /// Get or set resource using specific culture.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public string this[string key, CultureInfo culture]
        {
            get { return GetText(key, culture); }
            set { AddText(key, culture, value); }
        }

        private void RefreshCache(string key, CultureInfo culture, string value)
        {
            var val = Storage.Get(key);
            if (val == null || val.Count == 0)
            {
                Storage.Add(key, new Dictionary<CultureInfo, string>() {
                    {culture, value }
                });
            }
            else if (val.ContainsKey(culture))
            {
                val[culture] = value;
            }
            else
            {
                val.Add(culture, value);
            }
        }

        public void Dispose()
        {
            Storage.Dispose();
            DbContext.Dispose();
        }
    }
}
